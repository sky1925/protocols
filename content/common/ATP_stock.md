---
title: "ATP stock solution preparation"
---

Our lab has many forms of ATP, so the first thing to make sure is that you take the correct bottle.
We currently use ATP disodium salt hydrate from Sigma-Aldrich (A2383), 
which is stored in large -20 fridge.

To make 1 mL, ~200 mM ATP stock
1. Prepare a 25 mM Tris-HCl buffer, adjust pH to 10 using 10 N NaOH.
2. (Quickly!) Weigh 0.1102 g ATP disodium salt (MW = 551.14) and dissolve it in 
   the buffer made in step 1. 
   **Put the solution on ice.**
3. Adjust pH to 8.0 by adding 10 N NaOH, use pH paper as indicator.
4. Quantify by absorption at 259 nm, the extinction coefficient ε = 15.4 x 10<sup>3</sup> M<sup>-1</sup>. 
   Make two 100x serial dilution so that we have 100 uL 1/10000x solution to measure.
   Add the 100 uL 1/10000x solution to our thin quartz cell for Shimadzu spectrophotometer.
   Use spectra mode.
   Blank using the 25 mM Tris-HCl buffer from 240 nm to 280 nm.
   Measure the adsorption spectrum from 240 nm to 280 nm to check if the shape is correct.
   Calculate the concentration as A259/(15.4e3)\*10000.
5. Aliquot the solution into 3 uL. 
   Store them at -80 in a **separate** box, 
   labeled with the concentration, the size of aliquot, and the date of preparation.
   You can leave part of the solution to be aliquoted later (as it will be hundreds of tubes),
   but also store them at -80.

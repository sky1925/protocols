---
title: General buffer preparation guideline
---
= General buffer preparation guideline

== Pre-preparation

. *Check water*: Check out if there is enough water in the lab for this preparation.
+
[NOTE]
====
Most of the solutions whose volume is below 50 mL should be prepared
with autoclaved and filtered ddH~2~O; for large volume, use ddH~2~O.
*Never ever use tap water to prepare!!*
====

. *Find the solute*: Take out all the chemicals from where they were
stored, and put them on your bench. 
+
[NOTE]
====
Based on every chemical
condition, *if the chemical is stored in the -20 refrigerator, it is
plausible that they are very reactive to water. If so, please put them
on the bench until it reaches to RT.*
*This rule applies to all chemicals in -20.*
====


. *Check that you added all the Solute*: Line the needed chemical from
left to right *according* to the amount you need in the solution.

. *Weigh them*: Weigh the chemicals in the balance. Check the molecular
weight on the bottle and calculate accordingly. Several containers are
for different weight in need. *If the amount is below 1 g, you can use a
piece of weighing paper. For amount over 10 g, use the weighing pan.*
They are both located in the same drawer.

. *Reduce the error*: In order to achieve minimum error, when it almost
reaches the required weight, you can gently pat your hands holding the
spoon and shiver down the chemical on the spoon. Also, once the chemical
is taken out from its bottle, *it is severely unforgivable to put back
into the bottle. It will pollute the whole storage.*

== Dissolution

. *Choose the right container:* Several containers are suitable to
dissolve the solute, but *not volumetric flasks or graduated cylinders.*
Based on the inventory of our Lab chemical, most of the chemicals are
not very reactive. Therefore, most of the dissolution can be accompanied
by some heating or vortex.
+
[NOTE]
====
For our biochemical reaction, we can tolerate more concentration
errors than analytical chemists. Most of us use graduated tubes as
containers.
====

. *Add the solvent:* Add the solvent to the container until it reaches
approximately *80% of the final volume* since, during the dissolving
process, it is probable that the volume changes. The pH adjustment also
changes the volume of the solution.

. *Accelerate the dissolution:* For Erlenmeyer flasks and beakers, put a
stir bar to speed the dissolving process. As for centrifuge tubes,
vortex or put it into the oven can *give* the same effect.
+
[NOTE]
====
(Notice: Chemicals like potassium hydroxide or sodium hydroxide can
release heat during the dissolution. Make sure the container is
*surrounded by ice.*)
====

. *Adjust the pH:* For biological experiments, pH is by far one of the
most important factors. Therefore, it is an art to make sure your pH is
right at your *expectations*. NaOH and HCl are in the hood. Because they
are strong base and acid, do not take them out of the hood. All the pH
adjustments should be done in the hood. After completing the pH
adjustment, *make sure to find at least one member to double-check the
pH.*
. *Fill to full volume:* After making sure the pH, fill the solvent to
the desired volume. *This* process should be done by only a graduated
cylinder or a centrifuge tube. If the solution is used for biological
assays, filtering through 0.22 is mandatory.

== Storage

. After the solutes are completely dissolved, some notes are required on
the bottle, including the buffer name, concentration, date, component
concentrations, pH, filtered or not, and initials of the person who
prepared the buffer.
+
Here is an example:
image:/images/buffer_label_example.png[Buffer label example]

. For gel electrophoresis usage, the buffers are stored under room
temperature. The personal buffers used in biological reactions should be
stored at 4 °C or lower temperature and seal with parafilm. For
personal preference, some buffers should be stored at -20 °C and be
aliquoted in case of denaturation during repeating freeze/thaw cycles.
. Fill up lab ddH~2~O tank at *Prof. CCH Lab*, if it runs out.
